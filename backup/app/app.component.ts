import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { UsermanService } from './userman.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [UsermanService]
})
export class AppComponent {

  constructor() { }

}
