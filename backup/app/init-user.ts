export class Init {
    load() {
        if (localStorage.getItem('DataArray') === null || localStorage.getItem('DataArray') == undefined) {
            console.log('No users found...creating..')
            var DataArray = [
                { id: 12345, name: 'Lala', status: 'lagi ngoding', email: 'lala@ymail.com' },
                { id: 76876, name: 'Kiki', status: 'lagi ngoding juga', email: 'kiki@ymail.com' },
                { id: 89799, name: 'Cucung', status: 'lagi ngoding sampe malem', email: 'hhhh@yahoo.com' }
            ];

            localStorage.setItem('DataArray', JSON.stringify(DataArray));
            return
        } else {
            console.log('Found user');
        }
    }
}