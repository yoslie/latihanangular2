import { Routes, RouterModule } from '@angular/router';

import { WorkspaceComponent } from './workspace.component';

const workspaceRoutes: Routes = [
    { path: 'workspace', component: WorkspaceComponent, pathMatch: 'full' }
];

export const workspaceRouting = RouterModule.forChild(workspaceRoutes);