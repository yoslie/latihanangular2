import { Injectable } from '@angular/core';
import { Router, CanActivate, RouterModule } from '@angular/router';
import { tokenNotExpired } from 'angular2-jwt';


@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private routing: Router) {}

  canActivate() {
    if (tokenNotExpired()) {
      return true;
    }

    this.routing.navigate(['/login']);
    return false;
  }
}
