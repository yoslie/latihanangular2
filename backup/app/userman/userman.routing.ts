import { Routes, RouterModule } from '@angular/router';

import { UsermanComponent } from './userman.component';
import { WorkspaceComponent } from './../workspace/workspace.component';

const usermanRoutes: Routes = [
    { path: 'userman', component: UsermanComponent, pathMatch: 'full' },
    { path: 'workspace', component: WorkspaceComponent, pathMatch: 'full' },
];

export const usermanRouting = RouterModule.forChild(usermanRoutes);
