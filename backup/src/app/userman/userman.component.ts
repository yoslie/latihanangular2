import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { UsermanService } from '../userman.service';

@Component({
  selector: 'app-userman',
  templateUrl: './userman.component.html',
  styleUrls: ['./userman.component.scss']
})
export class UsermanComponent implements OnInit {
  isAddingNewPerson: boolean = false;
  isEditNewPerson: boolean = false;
  title = 'User Management';
  public DataArray: any;
  public myForm: FormGroup;
  appState= 'default';
  constructor(private _usermanService: UsermanService) {
    this.myForm = new FormGroup({
      id: new FormControl(''),
      name: new FormControl(''),
      email: new FormControl(''),
      status: new FormControl('')
    });
  }

  Added() {
    let formValue = this.myForm.getRawValue();
    
    var newUser = {
      id: formValue.id,
      name: formValue.name,
      email: formValue.email,
      status: formValue.status
    }

    this.DataArray.push(newUser);

    this._usermanService.addUser(newUser);

    this.myForm.reset();
  }

  Edit(formData) {
    this.appState = 'edit';
    this.myForm.controls['id'].setValue(formData.id);
    this.myForm.controls['name'].setValue(formData.name);
    this.myForm.controls['email'].setValue(formData.email);
    this.myForm.controls['status'].setValue(formData.status);
  }

  Update() {
    let formValue = this.myForm.getRawValue();
    let index = this.DataArray.findIndex(data => data.id == formValue.id);

    this.DataArray[index].id = formValue.id;
    this.DataArray[index].name = formValue.name;
    this.DataArray[index].email = formValue.email;
    this.DataArray[index].status = formValue.status;

    this._usermanService.updateUser();

    this.myForm.reset();
  }

  Delete(dataForm) {
    let index = this.DataArray.findIndex(data => dataForm.id);

    this.DataArray.splice(index, 1);
  }

  ngOnInit() {
    this.DataArray = this._usermanService.getUsers();
  }

}
