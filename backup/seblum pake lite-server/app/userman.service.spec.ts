/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { UsermanService } from './userman.service';

describe('UsermanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UsermanService]
    });
  });

  it('should ...', inject([UsermanService], (service: UsermanService) => {
    expect(service).toBeTruthy();
  }));
});
