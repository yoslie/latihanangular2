import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-userman',
  templateUrl: './userman.component.html',
  styleUrls: ['./userman.component.scss']
})
export class UsermanComponent implements OnInit {
  isAddingNewPerson:boolean = false;

  title = 'User Management';
  public DataArray: any;
  public myForm: FormGroup;
  constructor() {
    this.DataArray = [
      { id: 12345, name: 'Lala', status: 'lagi ngoding', email: 'lala@ymail.com' },
      { id: 76876, name: 'Kiki', status: 'lagi ngoding juga', email: 'kiki@ymail.com' },
      { id: 89799, name: 'Cucung', status: 'lagi ngoding sampe malem', email: 'hhhh@yahoo.com' }
    ];

    this.myForm = new FormGroup({
      id: new FormControl(''),
      name: new FormControl(''),
      email: new FormControl(''),
      status: new FormControl('')
    });
  }

  Added() {
    let formValue = this.myForm.getRawValue();

    this.DataArray.push({
      id: formValue.id,
      name: formValue.name,
      email: formValue.email,
      status: formValue.status
    });

    this.myForm.reset();
  }

  Edit(formData) {
    this.myForm.controls['id'].setValue(formData.id);
    this.myForm.controls['name'].setValue(formData.name);
    this.myForm.controls['email'].setValue(formData.email);
    this.myForm.controls['status'].setValue(formData.status);
  }

  Update() {
    let formValue = this.myForm.getRawValue();
    let index = this.DataArray.findIndex(data => data.id == formValue.id);

    this.DataArray[index].id = formValue.id;
    this.DataArray[index].name = formValue.name;
    this.DataArray[index].email = formValue.email;
    this.DataArray[index].status = formValue.status;

    this.myForm.reset();
  }

  Delete(dataForm) {
    let index = this.DataArray.findIndex(data => dataForm.id);

    this.DataArray.splice(index, 1);
  }

  ngOnInit() {
  }

}
