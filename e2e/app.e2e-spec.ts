import { AngularrestPage } from './app.po';

describe('angularrest App', function() {
  let page: AngularrestPage;

  beforeEach(() => {
    page = new AngularrestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
