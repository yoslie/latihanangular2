import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { UsermanComponent } from './userman/userman.component';
import { UsermanModule } from './userman/userman.module';
import { HomeComponent } from './home/home.component';
import { WorkspaceComponent } from './workspace/workspace.component';
import { WorkspaceModule } from './workspace/workspace.module';
import { routing } from './app.routing';
import { usermanRouting } from './userman/userman.routing';
import { NumberingPipe } from './numbering.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    UsermanComponent,
    HomeComponent,
    WorkspaceComponent,
    NumberingPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    UsermanModule,
    usermanRouting,
    routing,
    WorkspaceModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
