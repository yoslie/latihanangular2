import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

import * as _ from 'underscore';

import { UsermanService } from '../_services/userman.service';
import { PagerService } from '../_services/pager.service';

@Component({
  selector: 'app-userman',
  templateUrl: './userman.component.html',
  styleUrls: ['./userman.component.scss']
})
export class UsermanComponent implements OnInit {
  isAddingNewPerson: boolean = false;
  isEditNewPerson: boolean = false;
  title = 'User Management';
  public DataArray: any;
  public myForm: FormGroup;
  appState = 'default';
  constructor(private http: Http, private pagerService: PagerService, private _usermanService: UsermanService) {
    this.myForm = new FormGroup({
      id: new FormControl(''),
      name: new FormControl(''),
      email: new FormControl(''),
      status: new FormControl('')
    });
  }

  // array of all items to be paged
  private allItems: any[];
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];

  Added() {
    let formValue = this.myForm.getRawValue();

    var newUser = {
      id: formValue.id,
      name: formValue.name,
      email: formValue.email,
      status: formValue.status
    }

    this.DataArray.push(newUser);

    this._usermanService.addUser(newUser);

    this.myForm.reset();
  }

  Edit(formData) {
    this.appState = 'edit';
    this.myForm.controls['id'].setValue(formData.id);
    this.myForm.controls['name'].setValue(formData.name);
    this.myForm.controls['email'].setValue(formData.email);
    this.myForm.controls['status'].setValue(formData.status);
  }

  Update() {
    let formValue = this.myForm.getRawValue();
    let index = this.DataArray.findIndex(data => data.id == formValue.id);

    this.DataArray[index].id = formValue.id;
    this.DataArray[index].name = formValue.name;
    this.DataArray[index].email = formValue.email;
    this.DataArray[index].status = formValue.status;

    // this._usermanService.updateUser(this.formValue, this.DataArray);

    this.myForm.reset();
  }

  Delete(dataForm) {
    let index = this.DataArray.findIndex(data => data.id == dataForm.id);

    this.DataArray.splice(index, 1);

    this._usermanService.deleteUser(dataForm);

    this.myForm.reset();
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this.pagerService.getPager(this.allItems.length, page);

    // get current page of items
    this.DataArray = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  ngOnInit() {
    this.DataArray = this._usermanService.getUsers();

    // get dummy data
    this.http.get('./dummy-data.json')
      .map((response: Response) => response.json())
      .subscribe(data => {
        // set items to json response
        this.allItems = this.DataArray;

        // initialize to page 1
        this.setPage(1);
      });
  }
}
