export class Init {
    load() {
        if (localStorage.getItem('DataArrayW') === null || localStorage.getItem('DataArrayW') == undefined) {
            console.log('No workspace found...creating..')
            var DataArrayW = [
                { id: 111, name: 'eBdesk admin', number: '120'},
                { id: 222, name: 'eBdesk malaysia', number: '100'},
                { id: 333, name: 'Metro', number: '45'}
            ];

            localStorage.setItem('DataArrayW', JSON.stringify(DataArrayW));
            return
        } else {
            console.log('Found workspace');
        }
    }
}