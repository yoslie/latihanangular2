import { Injectable } from '@angular/core';
import { Init } from './init-workspace';

@Injectable()
export class WorkspaceService extends Init {

  constructor() {
    super();
    console.log('Workspace service initialized...');
    this.load();
  }

  getWorkspace() {
    var DataArrayW = JSON.parse(localStorage.getItem('DataArrayW'));
    return DataArrayW;
  }

  addWorkspace(newWorkspace) {
    var DataArrayW = JSON.parse(localStorage.getItem('DataArrayW'));
    // Add new workspace
    DataArrayW.push(newWorkspace);
    // Set new workspace
    localStorage.setItem('DataArrayW', JSON.stringify(DataArrayW));
  }

  deleteWorkspace(dataForm) {
    var DataArrayW = JSON.parse(localStorage.getItem('DataArrayW'));
    let index = DataArrayW.findIndex(data => data.id == dataForm.id);

    DataArrayW.splice(index, 1);

    localStorage.setItem('DataArrayW', JSON.stringify(DataArrayW));
  }

  updateWorkspace(dataForm, formValue) {
    var DataArrayW = JSON.parse(localStorage.getItem('DataArrayW'));
    let index = DataArrayW.findIndex(data => data.id == dataForm.id);

    localStorage.setItem('DataArrayW', JSON.stringify(DataArrayW));
  }

}
