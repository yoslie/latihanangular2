import { Injectable } from '@angular/core';
import { Init } from './init-user';

@Injectable()
export class UsermanService extends Init {

  constructor() {
    super();
    console.log('Userman service initialized...');
    this.load();
  }

  getUsers() {
    var DataArray = JSON.parse(localStorage.getItem('DataArray'));
    return DataArray;
  }

  addUser(newUser) {
    var DataArray = JSON.parse(localStorage.getItem('DataArray'));
    // Add new user
    DataArray.push(newUser);
    // Set new user
    localStorage.setItem('DataArray', JSON.stringify(DataArray));
  }

  deleteUser(dataForm) {
    var DataArray = JSON.parse(localStorage.getItem('DataArray'));
    let index = DataArray.findIndex(data => data.id == dataForm.id);

    DataArray.splice(index, 1);

    localStorage.setItem('DataArray', JSON.stringify(DataArray));
  }

  updateUser(dataForm, formValue) {
    var DataArray = JSON.parse(localStorage.getItem('DataArray'));
    let index = DataArray.findIndex(data => data.id == dataForm.id);

    localStorage.setItem('DataArray', JSON.stringify(DataArray));
  }

}
