import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { UsermanService } from './_services/userman.service';
import { WorkspaceService } from './_services/workspace.service';
import {PagerService} from './_services/pager.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [UsermanService, WorkspaceService, PagerService]
})
export class AppComponent {

  constructor() { }

}
