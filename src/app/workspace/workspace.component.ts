import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

import * as _ from 'underscore';

import { WorkspaceService } from '../_services/workspace.service';
import { PagerService } from '../_services/pager.service';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.scss']
})
export class WorkspaceComponent implements OnInit {
  isAddingNewPerson: boolean = false;
  isEditNewPerson: boolean = false;
  title = 'Workspace Settings';
  public DataArrayW: any;
  public myForm: FormGroup;
  appState = 'default';
  constructor(private http: Http, private pagerService: PagerService, private _workspaceService: WorkspaceService) {
    this.myForm = new FormGroup({
      id: new FormControl(''),
      name: new FormControl(''),
      number: new FormControl('')
    });
  }

  // array of all items to be paged
  private allItems: any[];
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];

  Added() {
    let formValue = this.myForm.getRawValue();

    var newWorkspace = {
      id: formValue.id,
      name: formValue.name,
      number: formValue.number
    }

    this.DataArrayW.push(newWorkspace);

    this._workspaceService.addWorkspace(newWorkspace);

    this.myForm.reset();
  }

  Edit(formData) {
    this.appState = 'edit';
    this.myForm.controls['id'].setValue(formData.id);
    this.myForm.controls['name'].setValue(formData.name);
    this.myForm.controls['number'].setValue(formData.number);
  }

  Update() {
    let formValue = this.myForm.getRawValue();
    let index = this.DataArrayW.findIndex(data => data.id == formValue.id);

    this.DataArrayW[index].id = formValue.id;
    this.DataArrayW[index].name = formValue.name;
    this.DataArrayW[index].number = formValue.number;

    // this._workspaceService.updateUser(this.formValue, this.DataArray);

    this.myForm.reset();
  }

  Delete(dataForm) {
    let index = this.DataArrayW.findIndex(data => data.id == dataForm.id);

    this.DataArrayW.splice(index, 1);

    this._workspaceService.deleteWorkspace(dataForm);

    this.myForm.reset();
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this.pagerService.getPager(this.allItems.length, page);

    // get current page of items
    this.DataArrayW = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  ngOnInit() {
    this.DataArrayW = this._workspaceService.getWorkspace();

    // get dummy data
    this.http.get('./dummy-data.json')
      .map((response: Response) => response.json())
      .subscribe(data => {
        // set items to json response
        this.allItems = this.DataArrayW;

        // initialize to page 1
        this.setPage(1);
      });
  }

}
