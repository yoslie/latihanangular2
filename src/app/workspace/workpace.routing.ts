import { Routes, RouterModule } from '@angular/router';

import { UsermanComponent } from './../userman/userman.component';
import { WorkspaceComponent } from './workspace.component';

const workspaceRoutes: Routes = [
    { path: 'userman', component: UsermanComponent, pathMatch: 'full' },
    { path: 'workspace', component: WorkspaceComponent, pathMatch: 'full' }
];

export const workspaceRouting = RouterModule.forChild(workspaceRoutes);